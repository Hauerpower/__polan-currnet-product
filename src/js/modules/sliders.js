export  default function slidersInit(){
	
	(function($){
	
		$(document).ready(function(){
			$('.small-item-thumbs').slick({
				slidesToShow: 4,
				slidesToScroll: 1,
				dots: false,
				arrows: false,
				vertical: true,
				asNavFor: '.big-item-thumbs',
				focusOnSelect: true,
				infinite: true,
				autoplay: true
			  });
			  $('.big-item-thumbs').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: false,
				arrows: false,
				asNavFor: '.small-item-thumbs',
				focusOnSelect: true,
				fade: true,
				cssEase: 'linear',
				speed: 1000,
				infinite: true
			  });
			  $('.related-wrapper').slick({
				arrows: true,
				infinite: true,
				speed: 300,
				slidesToShow: 5,
				slidesToScroll: 1,
				responsive: [
					{
						breakpoint: 1224,
						settings: {
						  slidesToShow: 4,
						  slidesToScroll: 1,

						  arrows: true
						}
					  },
					{
						breakpoint: 768,
						settings: {
						  slidesToShow: 3,
						  slidesToScroll: 1,

						  arrows: true
						}
					  },
				  {
					breakpoint: 640,
					settings: {
					  slidesToShow: 2,
					  slidesToScroll: 1,
					  arrows: true
					}
				  },
				  {
					breakpoint: 480,
					settings: {
					  slidesToShow: 2,
					  slidesToScroll: 1,
					  arrows: true
					}
				  }
				  // You can unslick at a given breakpoint now by adding:
				  // settings: "unslick"
				  // instead of a settings object
				]
			  });
		  });

	})(jQuery);
	
}
 
// $('.slider-for').slick({
// 	slidesToShow: 1,
// 	slidesToScroll: 1,
// 	arrows: false,
// 	fade: true,
// 	asNavFor: '.slider-nav'
//   });
//   $('.slider-nav').slick({
// 	slidesToShow: 3,
// 	slidesToScroll: 1,
// 	asNavFor: '.slider-for',
// 	dots: true,
// 	centerMode: true,
// 	focusOnSelect: true
//   });