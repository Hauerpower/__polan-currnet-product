<?php
	

function atom_translate_register(){
  if(function_exists('pll_register_string')) {
    $strings = get_option('atom_translated_strings');
    if($strings){
      foreach($strings as $string) {
        pll_register_string(THEME_NS, $string, THEME_NS);
      }
    }
  }

}
add_action('after_setup_theme', 'atom_translate_register');


	
function atom_add_to_translate($string){
	$translated_strings = get_option('atom_translated_strings', array());
	if( !$translated_strings ) $translated_strings = array();
	
	if( !in_array($string, $translated_strings) ){
		$translated_strings[] = $string;
		update_option('atom_translated_strings', $translated_strings);
	}
	
}

function a_e($string){
	if(function_exists('pll_e')) {
		atom_add_to_translate($string);
		pll_e($string);
	}
	else {
		echo $string;
	}
}
function a__($string){
	if(function_exists('pll__')) {
		atom_add_to_translate($string);
		return pll__($string);
	}
	return $string;
}